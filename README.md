# Files for organising a Workshop

Created by Alexander Brune (gitlab: @acb2018, email: alexander.brune@gmail.com) and modified by Gianmarco Brocchi (gitlab: @gimmy, gianmarcobrocchi@gmail.com).


To compile the Book of Abstracts run `make` from the directory TeXsources.
Use `make clean` to remove temporary files.
Use `make erase` to remove aux and PDF files. 


## Book of abstracts

* abstracts.tex : main file to compile

* titlepage.tex (tex-file used in online-version abstracts.tex)

* titlepage_selfcontained.tex (pdf-file used in print-version of abstracts.tex, needs to be typeset first)

* schedule.tex  (pdf-file used in abstracts.tex, needs to be typeset first)

* schedule_table.tex (tex-file used in schedule.tex)

* make_bibs (bash script, see comments on bibliographies in abstracts.tex)

* folder: images


### Name tags:

* nametags.tex

* nametag_content.tex (tex-file used in nametags.tex)
* make_nametag_content.py (creates nametag_content.tex from csv-file)


# Optional files in this folder
-----------------------------

* schedule_A3.tex (to hang up schedule outside of lecture rooms or wherever)
* signs

-----------------------------
